#! /bin/bash

# SPDX-FileCopyrightText: 2018-2019 Michael Pöhn <michael@poehn.at>
# SPDX-License-Identifier: GPL-3.0-or-later

# fetch newest image
docker pull louislam/uptime-kuma:1

# check if pull image is an update, if not, exit
if [ "$(docker inspect --format='{{.Id}}' louislam/uptime-kuma:1)" = "$(docker inspect --format='{{.Image}}' uptime-kuma)" ]; then
    echo "Already up-to-date"
    exit 0
fi

# try nuke previously started container
docker stop uptime-kuma
docker rm  uptime-kuma

# start new container
docker run --detach --restart=always --name uptime-kuma --publish 127.0.0.1:3001:3001 --volume uptime-kuma:/app/data --add-host=host.docker.internal:host-gateway louislam/uptime-kuma:1

# nuke outdated docker images
OLD=$(docker images | grep -v uptime-kuma | grep -v REPOSITORY | awk '{print $3}')
for O in $OLD; do
    docker rmi "$O"
done
