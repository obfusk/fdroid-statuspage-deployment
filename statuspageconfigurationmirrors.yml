# SPDX-FileCopyrightText: 2023 Sylvia van Os <sylvia@hackerchick.me>
# SPDX-License-Identifier: GPL-3.0-or-later
---
- name: "monitor: configure group for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    name: "{{ item.name }}"
    parent_name: "Mirrors"
    type: "group"
    interval: "{{ item.interval | default(3600) }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: set tags for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor_tag:
    api_token: "{{ api_token }}"
    monitor_name: "{{ item.name }}"
    tag_name: "{{ tag.name }}"
    state: "{{ (tag.enabled) | ternary('present', 'absent') }}"
  loop:
    - {name: "official", enabled: "{{ item.official | default(false) }}"}
    - {name: "archive (v1)", enabled: "{{ item.archive.v1 | default(true) }}"}
    - {name: "archive (v2)", enabled: "{{ item.archive.v2 | default(true) }}"}
    - {name: "main (v1)", enabled: "{{ item.main.v1 | default(true) }}"}
    - {name: "main (v2)", enabled: "{{ item.main.v2 | default(true) }}"}
  loop_control:
    loop_var: tag
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v1 archive for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v1 (Archive)"
    url: "{{ item.base_url }}/archive/index-v1.jar"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "HEAD"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "http"
    state: "{{ (item.archive.v1 | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v1 main for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v1 (Main)"
    url: "{{ item.base_url }}/repo/index-v1.jar"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "HEAD"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "http"
    state: "{{ (item.main.v1 | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v2 archive for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v2 (Archive)"
    url: "{{ item.base_url }}/archive/index-v2.json"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "HEAD"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "http"
    state: "{{ (item.archive.v2 | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v2 main for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v2 (Main)"
    url: "{{ item.base_url }}/repo/index-v2.json"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "HEAD"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "http"
    state: "{{ (item.main.v2 | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v2 diff archive for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v2 diff (Archive)"
    url: "{{ item.base_url }}/archive/entry.jar"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "HEAD"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "http"
    state: "{{ (item.archive.v2_diff | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v2 diff main for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v2 diff (Main)"
    url: "{{ item.base_url }}/repo/entry.jar"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "HEAD"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "http"
    state: "{{ (item.main.v2_diff | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v2 entry.json timestamp archive for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v2 entry.json (Archive)"
    url: "{{ item.base_url }}/archive/entry.json"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "GET"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "json-query"
    jsonPath: >-
      ($millis() - timestamp < 14*24*3600*1000)
      ? "up-to-date"
      : "more than 14 days old"
    expectedValue: "up-to-date"
    state: "{{ (item.archive.v2_diff | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager

- name: "monitor: configure index-v2 entry.json timestamp main for mirror {{ item.name }}"
  lucasheld.uptime_kuma.monitor:
    api_token: "{{ api_token }}"
    parent_name: "{{ item.name }}"
    name: "{{ item.name }} index-v2 entry.json (Main)"
    url: "{{ item.base_url }}/repo/entry.json"
    proxy: "{{ item.proxy | default(omit) }}"
    method: "GET"
    interval: "{{ item.interval | default(3600) }}"
    retryInterval: "{{ item.retryInterval | default(3600) }}"
    maxretries: "{{ item.maxretries | default(5) }}"
    notification_names: "{{ (notifications and (item.official | default(false))) | ternary(notifications, []) }}"
    type: "json-query"
    jsonPath: >-
      ($millis() - timestamp < 14*24*3600*1000)
      ? "up-to-date"
      : "more than 14 days old"
    expectedValue: "up-to-date"
    state: "{{ (item.main.v2_diff | default(true)) | ternary('present', 'absent') }}"
  register: result
  until: result is not failed
  retries: 10
  become_user: uptime-kuma-api-manager
