<!--
SPDX-FileCopyrightText: 2023 Sylvia van Os <sylvia@hackerchick.me>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# F-Droid Uptime Kuma Deployment

NB: when deploying to staging instead of production, replace `fdroidStatus` with `fdroidStatusStaging` (and the username/password with those for staging as well).

## deploy

    ansible-galaxy install -f -r requirements.yml -p .galaxy
    ansible-playbook -i fdroidStatus, webserver.yml
    ansible-playbook -i fdroidStatus, statuspagecontainer.yml
    ansible-playbook -i fdroidStatus, torproxy.yml

## config

First, make a Matrix notifier under [Notifications](https://fdroidstatus.org/settings/notifications) with the name `Matrix: #fdroid-status-alerts:matrix.org`. This is not done automatically because it contains secrets. If you forget that, you'll get weird errors running the configuration.

Second, install the requirements (you also need to re-run this if these are updated):

    ansible-galaxy collection install -r requirements.yml -p .galaxy-collections

You can then run the configuration job. Optionally, you can run only specific tasks or skip specific tasks using tags. To list the available tags, run the following command:

    ansible-playbook -i fdroidStatus, statuspageconfiguration.yml --list-tags

To run all deployment steps, use the following command:

    ansible-playbook -i fdroidStatus, statuspageconfiguration.yml -e "api_username=xxxxx api_password=xxxxx api_2fa=xxxxx"

You can also put `api_username` and `api_password` in a YAML file and use e.g. `-e @/path/to/production.yml` instead of passing the credentials directly to the command (make sure to store this file outside of the git repo and not accidentally commit the credentials if you do):

```yaml
api_username: xxxxx
api_password: xxxxx
```

**Note: running the full configuration job will take about 1.5 hours and it is not able to configure the icon on https://fdroidstatus.org/status/fdroid, you'll have to upload that manually.**

## updates

The Uptime Kuma docker container will update automatically to the latest 1.x version.  We will need to keep `ansible-uptime-kuma` (in `requirements.yml`) and `uptime-kuma-api` (in `requirements.txt`) up to date to match.  Support for the latest Uptime Kuma may lag behind by a week or so; point releases should be backwards compatible (and 1.23.x should be the last releases before 2.x).

Updating/generating `requirements.txt` is done by installing `uptime-kuma-api` in a clean venv and running `pip freeze` to keep deployments consistent:

```sh
$ python3 -mvenv venv
$ . venv/bin/activate
$ pip install uptime-kuma-api==1.2.1
$ pip freeze > requirements.txt
```
